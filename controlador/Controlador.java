/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import modelo.Producto;
import vista.dlgProductos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


/**
 *
 * @author patrick
 */

public class Controlador  implements ActionListener{
    
    private Producto pro;
    private dlgProductos vista;

    public Controlador(Producto pro, dlgProductos vista) {
        this.pro = pro;
        this.vista = vista;
        
        // Hacer que el controlador ESCUCHE los botones de la vista
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        
    }
    
    private void iniciarVista(){
    
    vista.setTitle("::  Productos  ::");
    vista.setSize(500,500);
    vista.setVisible(true);
    
    }
    
    
    public void limpiar(){
    
          vista.txtCantidad.setText("");
            vista.txtCodigo.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPcompra.setText("");
            vista.txtPventa.setText("");
            vista.txtTotalCompra.setText("");
            vista.txtTotalVta.setText("");
            vista.txtUnidad.setText("");
            vista.txtTotalGanancia.setText("");

    
    
    
    
    
    }
    
     @Override
    public void actionPerformed(ActionEvent e) {
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   
    if( e.getSource()==vista.btnLimpiar){
    // codificar el boton de limpiar
    
           limpiar();
  
    }
    
    if (e.getSource()==vista.btnNuevo){
    
    // activar 
    
     vista.txtCantidad.setEnabled(true);
            vista.txtCodigo.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.txtPcompra.setEnabled(true);
            vista.txtPventa.setEnabled(true);
         
            vista.txtUnidad.setEnabled(true);
            vista.txtTotalGanancia.setEnabled(true);
           vista.btnGuardar.setEnabled(true);
           vista.btnMostrar.setEnabled(true);
    
    }
    
    if(e.getSource()==vista.btnGuardar){
    
        pro.setCodigo(vista.txtCodigo.getText());
        pro.setDescripcion(vista.txtDescripcion.getText());
        pro.setUnidadMedida(vista.txtUnidad.getText());
        
        try {
                pro.setPrecioCompra(Float.parseFloat(vista.txtPcompra.getText()));
                pro.setPrecioVenta(Float.parseFloat(vista.txtPventa.getText()));
                pro.setCantidad(Integer.parseInt(vista.txtCantidad.getText()));
                JOptionPane.showMessageDialog(vista, "Se agrego exitosamente");
                limpiar();
        } catch(NumberFormatException ex){
        
            JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex.getMessage());

        } 
           catch(Exception ex2){
           
           JOptionPane.showMessageDialog(vista, "Surgio el siguiente error " + ex2.getMessage());
           }
     
    }
    if(e.getSource() == this.vista.btnMostrar){
        this.vista.txtCodigo.setText(this.pro.getCodigo());
        this.vista.txtDescripcion.setText(this.pro.getDescripcion());
        this.vista.txtUnidad.setText(this.pro.getUnidadMedida());
        this.vista.txtPcompra.setText(String.valueOf(this.pro.getPrecioCompra()));
        this.vista.txtPventa.setText(String.valueOf(this.pro.getPrecioVenta()));
        this.vista.txtCantidad.setText(String.valueOf(this.pro.getCantidad()));
        this.vista.txtTotalCompra.setText(String.valueOf(this.pro.calcularTotalCompra()));
        this.vista.txtTotalVta.setText(String.valueOf(this.pro.calcularTotalVenta()));
        this.vista.txtTotalGanancia.setText(String.valueOf(this.pro.calcularTotalGanancia()));
    }
    if(e.getSource()==this.vista.btnCerrar){
        int option=JOptionPane.showConfirmDialog(vista, "¿Deseas salir?",
                "Decide", JOptionPane.YES_NO_OPTION);
        if(option==JOptionPane.YES_NO_OPTION){
            vista.dispose();
            System.exit(0);
        }
       }
    
    
    if(e.getSource()==this.vista.btnCancelar) {
        this.vista.btnGuardar.setEnabled(false);
        this.vista.btnMostrar.setEnabled(false);

        limpiar();
        
        
        this.vista.txtCodigo.setEnabled(false);
        this.vista.txtDescripcion.setEnabled(false);
        this.vista.txtUnidad.setEnabled(false);
        this.vista.txtPcompra.setEnabled(false);
        this.vista.txtPventa.setEnabled(false);
        this.vista.txtCantidad.setEnabled(false);
        
    }
    
    }
    
    
    
    public static void main(String[] args) {
        
        // TODO code application logic here
        
        
        Producto pro = new Producto();
        dlgProductos vista = new  dlgProductos(new JFrame(),true);
        
        Controlador contra = new Controlador(pro,vista);
        contra.iniciarVista();
        
    }

   
}
