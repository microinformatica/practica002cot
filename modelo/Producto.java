package modelo;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author patrick
 */
public class Producto {
private String codigo;
private String descripcio;
private String unidadMedida;
private float precioCompra;
private float precioVenta;
private int cantidad;

    public Producto(String codigo, String sescripcio, String unidadMedida, float precioCompra, float precioVenta) {
        this.codigo = codigo;
        this.descripcio = sescripcio;
        this.unidadMedida = unidadMedida;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
        this.cantidad= cantidad;
    }

    public Producto() {
        
        
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the sescripcio
     */
    public String getDescripcion() {
        return descripcio;
    }

    /**
     * @param sescripcio the sescripcio to set
     */
    public void setDescripcion(String sescripcio) {
        this.descripcio = sescripcio;
    }

    /**
     * @return the unidadMedida
     */
    public String getUnidadMedida() {
        return unidadMedida;
    }

    /**
     * @param unidadMedida the unidadMedida to set
     */
    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    /**
     * @return the precioCompra
     */
    public float getPrecioCompra() {
        return precioCompra;
    }

    /**
     * @param precioCompra the precioCompra to set
     */
    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }

    /**
     * @return the precioVenta
     */
    public float getPrecioVenta() {
        return precioVenta;
    }

    /**
     * @param precioVenta the precioVenta to set
     */
    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }
    
    public void setCantidad(int cantidad){
        this.cantidad = cantidad;
    }
    
    public int getCantidad(){
        return this.cantidad;
    }
 
    
    public float calcularTotalCompra(){
        return this.cantidad * this.precioCompra;
    }
    
    public float calcularTotalVenta(){
        return this.cantidad * this.precioVenta;
    }
    
    public float calcularTotalGanancia(){
        return this.calcularTotalVenta() - this.calcularTotalCompra();
    }

}
